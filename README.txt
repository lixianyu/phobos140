Texas Instruments, Inc.

CC2540/41 Bluetooth Low Energy Software Development Kit
Release Notes

Version 1.4.0
November 8, 2013

Notices:

- This version of the Texas Instruments BLE stack and software is a minor 
  update to the v1.3.2 release. It contains some minor bug fixes and a few 
  functional changes.

- The BLE protocol stack, including both the controller and host, was 
  completely retested for v1.4.0.

Changes and Enhancements:

- All projects have been migrated from IAR v8.10.4 to IAR 8.20.2. In order to
  build all projects, be sure to upgrade to IAR v8.20.2.

- Updated SPI and UART_DMA drivers for improved robustness and throughput.

- Added an overlapped processing feature to improve throughput and reduce power
  consumption in devices where peak power consumption isn't an issue.    Overlapped processing allows the stack to concurrently process while the radio 
  is active.  Since the stack is concurrently processing, it is able to insert 
  new data in the Tx buffer during the connection event, causing additional 
  packets to be sent before the end of the event.

- Added a Number of Completed Packets HCI command which offers the possibility   
  of waiting for a certain number of completed packets before reporting to the   
  host. This allows higher throughput when used with overlapped processing.

- Added an HCI Extension command HCI_EXT_DelaySleepCmd which provides the user 
  control of the system initialization sleep delay (wake time from PM3/boot 
  before going back to sleep).  The default sleep delay is based on the 
  reference design 32 kHz XOSC stabilization time.

- Added a low duty cycle directed advertising option.

- Added support for deleting a single bond with the GAP_BondSetParam command.

- Decreased CRC calculation time during OAD by using DMA. 

Bug Fixes:

- Using a short connection interval and exercising high throughput, there was   
  some loss of packets.  This was fixed by adding host to application flow   control support.

- Bonding was unstable at short connection intervals. This is now fixed.

- Fixed USB CDC Drivers to work with Windows 8.

- OAD sample project would fail if long connection interval was used. This was     fixed by not allowing parameter updates to the central device.

- Fixed linking errors in UBL project.

- Fixed minor issues in sample apps to work with PTS dongle.

- Fixed USB descriptors in HostTestRelease to display correct string.

Known Issues:

- Use of the NV memory (to save application data or BLE Host bonding
  information) during a BLE connection may cause an unexpected disconnect.
  The likelihood of this happening increases with frequent usage, especially
  when using short connection intervals. The cause is related to the NV wear
  algorithm which at some point may cause an NV page erase which can disrupt
  system real-time processing. It is therefore recommended that the NV memory
  be used sparingly, or only when a connection is not active.

- HCI packet size of 128 bytes or more will be disregarded by the stack, and as 
  such, no HCI event will be returned.

For technical support please visit the Texas Instruments Bluetooth low energy
E2E Forum:

http://e2e.ti.com/support/low_power_rf/f/538.aspx

For additional sample applications, guides, and documentation, visit the Texas 
Instruments Bluetooth Low Energy wiki page at:

http://processors.wiki.ti.com/index.php/Category:BluetoothLE

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
